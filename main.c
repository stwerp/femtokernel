#include <msp430.h>
#include <stdlib.h>
#include "kernel.h"

// DEFINE MAX_TASKS in kernel.c!


//define tasks
void task1(void);
void task2(void);
void task3(void);


int main(){

    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer
    PM5CTL0 = 0xFFFE;           // Enable pins

    TaskInit(&task1,0); //Initialize task 1 & 2
    TaskInit(&task2,1);
    P1DIR |= BIT0;;
    P1OUT &= ~(BIT0);

    RunTask(0); // run tasks

    return 0;
}

/* user task #1 to blink LED*/
void task1(void){
    P1DIR |= BIT0; //set direction of pin to output
    while(1){
        P1OUT ^= BIT0; //toggle pin
    }
}

/* user task #2 if button pressed it turns on LED*/
void task2(void){
    unsigned int i = 0;
    //set pin to input and enable pull up resistor
    P1DIR &= ~(BIT2);
    P1REN |= (BIT2);
    P1OUT |= (BIT2);
    //set pin to output
    P9DIR |= BIT7;
    while(1){
        if(!(P1IN & (BIT2))){//check if button is pressed or not
            i++;
            if (i == 10){
                TaskInit(&task3,10);
            }
            P9OUT &= ~(BIT7); //turn on LED
        }
        else{
            P9OUT |= (BIT7); //turn off LED
        }
    }
}

void task3(void){
    //set pin to input and enable pull up resistor
    P1DIR &= ~(BIT1);
    P1REN |= (BIT1);
    P1OUT |= (BIT1);
    while ( (P1IN & (BIT1)) ){   };
    P1DIR ^= BIT0; // enable/disable button
    return 0;
}
