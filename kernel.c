#include <msp430.h>
#include <in430.h>
#include <stdint.h>
#include "kernel.h"

#ifndef MAX_TASK
#define MAX_TASK 10
#endif

// Global variable for pushing
extern uint16_t PushVAR = 0;

#define ASM_cPush asm(" push.w PushVAR\n")
#define ASM_cPop  asm(" pop.w PushVAR\n")

#define cPush(X)  ({\
    PushVAR = X;\
    ASM_cPush;\
    })
#define cPop(X)  ({\
    ASM_cPop;\
    X = PushVAR;\
    })

//task control block(tcb) base address for task id 0.
//set task size to 64 bytes
#define TASK_SIZE 128

#define RAMTOP 0x23FF
/* #define TCB_BASE 0x23c0 */
#define TCB_BASE (RAMTOP - TASK_SIZE)

//save context  
// INTERRUPT will push PC, then push SR
#define SAVE_CONTEXT() asm(\
            " push.w r4 \n"\
            " push.w r5 \n"\
            " push.w r6 \n"\
            " push.w r7 \n"\
            " push.w r8 \n"\
            " push.w r9 \n"\
            " push.w r10 \n"\
            " push.w r11 \n"\
            " push.w r12 \n"\
            " push.w r13 \n"\
            " push.w r14 \n"\
            " push.w r15 \n")

//restore context
#define RESTORE_CONTEXT() asm(\
            " pop.w r15 \n"\
            " pop.w r14 \n"\
            " pop.w r13 \n"\
            " pop.w r12 \n"\
            " pop.w r11 \n"\
            " pop.w r10 \n"\
            " pop.w r9 \n"\
            " pop.w r8 \n"\
            " pop.w r7 \n"\
            " pop.w r6 \n"\
            " pop.w r5 \n"\
            " pop.w r4 \n"\
            " reti \n")
// RETI is equivalent to "pop SR; pop PC")
			   


uint8_t CurrentTask=0; //track current running task
uint16_t Counter=0;

struct sTask Task[MAX_TASK];

void SchedulerInit(void){
	__disable_interrupt();
	//interrupt will be enabled on first context switch

	TA0CTL|=TACLR + MC_0;	//clear/reset and stop the timer A
	TA0CTL |= TASSEL_2 + ID_2;//Setting SMCLK as clock source
	TA0CCTL0 |= CCIE;	//enable compare interrupt
	TA0CCTL0 &= ~(CCIFG);	//clear compare interrupt flag
	TA0R=0;			//Timer value set to 0
	TA0CCR0=600;		//Compare register value for 10 ms period

	TA0CTL |= MC_1;		//Start timer A in up mode
}

inline void SwitchTask(void){
    // Increment counter, go to next task
    while(CurrentTask <= MAX_TASK){
        CurrentTask++;
        if(CurrentTask >= Counter){
            CurrentTask = 0;
        }
        if (Task[CurrentTask].state == ready){
            return;
        }
    }
    CurrentTask = 0; // in case of errors
}

/******************************
  scheduler with round robin algorithm
  It also performs context switching
******************************/
//void SchedulerISR(void) __attribute__((interrupt (TIMER0_A0_VECTOR),naked));
#pragma vector=TIMER0_A0_VECTOR
__interrupt __attribute__((naked)) void SchedulerISR(void){
	SAVE_CONTEXT(); 
	//save task's stack pointer
	Task[CurrentTask].prevStack = __get_SP_register();

	// Increment counter, go to next task
	SwitchTask();

	//load next task's stack pointer
	__set_SP_register( Task[CurrentTask].prevStack);
	RESTORE_CONTEXT();
}


void TaskComplete(void){
    __disable_interrupt();
    Task[CurrentTask].state = terminated;
    // reset CPU Tick
    TA0R=0;         //Timer value set to 0
    TA0CCTL0 &= ~(CCIFG);   //clear compare interrupt flag
    __enable_interrupt();
    SwitchTask();
    //load next task's stack pointer
    __set_SP_register( Task[CurrentTask].prevStack);
    RESTORE_CONTEXT();
}


void TaskSetState(enum TaskState state){
    Task[CurrentTask].state = state;
}

/******************************
  initialize tasks and their stack frame
******************************/
void TaskInit(void (*pFun)(void),uint16_t taskId){
    __disable_interrupt();
    // Store current PC to temporary variable
	register uint16_t temp;
	temp = __get_SP_register();

	register uint16_t id = (uint16_t) taskId;
	register uint16_t add = (uint16_t) pFun;
	register uint16_t loop;

	register uint16_t i = 0;
	while(i < Counter){
	    // re-use old TCB if its terminated
	    if (Task[i].state == terminated){
	        break;
	    }
	    i++;
	}
	if(i == Counter){
	    Counter = Counter + 1; // new task added
	    //i = Counter;
	}
	//use 'i' as our new task index

	// Initialize task structure
	Task[i].id=id;
	Task[i].address = add;
	Task[i].TCBLoc= (uint16_t)(TCB_BASE-(TASK_SIZE* i));
	Task[i].prevStack = __get_SP_register(); //0x3F6;
	Task[i].state = ready;

	 // Change Stack pointer to task TCB
	__set_SP_register( Task[i].TCBLoc);

	// PUSH RETURN ADDRESS FIRST for when tasks complete
	cPush( (uint16_t) &TaskComplete);
	// Push program Counter,Status register and more 12 registers
	cPush(add);
	cPush(GIE); // Enable interrupts on first context switch

	//initialize task stack frame with zero
	for(loop=0;loop<12;loop++){
	    cPush(0x0000);
	}
	
	// task TCB current stack pointer value is saved
	Task[i].prevStack = __get_SP_register();

	 // Back to previous stack value
	__set_SP_register(temp);
	__enable_interrupt();
}

/******************************
  run a task
******************************/
void RunTask(uint8_t taskId){
    CurrentTask=taskId; //set running task
    int i = 0;
    while(i < Counter){
        if (Task[i].id == taskId)
            break;
        i++;
    }
    if (i == Counter){
        i = 0;
    }
    SchedulerInit(); //Initialize timer
    __set_SP_register(Task[i].prevStack);
    RESTORE_CONTEXT(); //pop out task
}

